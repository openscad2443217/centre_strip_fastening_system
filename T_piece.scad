module t_piece(x,y,z_length,overstand){
  cube([x,y,z_length],center=true);
  translate([0,y/2,0]) cube([x+(overstand*2),2,z_length],center=true);
}
render(){
  t_piece(15,27.4,30,15);
  }
