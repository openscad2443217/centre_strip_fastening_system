module i_piece(x,y,z_length,overstand){
    cube([x,y,z_length],center=true);
    for(i=[-y/2,y/2]){
      translate([0,i,0]) cube([x+(overstand*2),2,z_length],center=true);
    }
}
render(){
  i_piece(15,27.4,30,15);
  }
