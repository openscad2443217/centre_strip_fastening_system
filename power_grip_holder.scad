module power_Grip_holder(){
  difference(){
    translate([0,-27,42]) cube([47.7,3,35],center=true);
    translate([0,-25,41]) rotate([90,0,0]) cylinder(d=18,h=10);
    }
    grip_holder(50);
  }
module grip_holder(z_length){
  difference(){
    union(){
      cube([73,57,z_length],center=true);
      translate([0,28,0]) cube([120,2,z_length],center=true);
      }
    translate([0,3.5,-1]) cube([69,58,z_length+3],center=true);
    }
  }
module table_cover(z){
  x=73;
  y=10;
  difference(){
    union(){
      cube([x,y,z],center=true);
      translate([0,y/2,0]) cube([120,2,z],center=true);
      }
      cube([x-20,y,z+2],center=true);
    }
  }
module i_piece(x,y,z_length,overstand){
    cube([x,y,z_length],center=true);
    for(i=[-y/2,y/2]){
      translate([0,i,0]) cube([x+(overstand*2),2,z_length],center=true);
      }
  }
render(){
//table_cover(300);
//power_Grip_holder();
//grip_holder(290);
i_piece(15,27.4,30,15);
  }
